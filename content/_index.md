---
title: Automotive Open Source Summit 2023
seo_title: Automotive Open Source Summit 2023
date: 2022-11-30T10:00:00-04:00
header_wrapper_class: "header-automotive-oss-event-2023"
summary: 'The Automotive Open Source Summit is an annual gathering of senior architects, innovators, and executives from around the world. At this event, attendees come together to discuss the latest trends in automotive grade open source software, network with like-minded professionals, and learn about the various technologies and challenges faced in automotive software development. As the inaugural event, the 2023 Automotive Open Source Summit will be focused around topics for senior architects and executives, with the outlook of expanding the conversation towards developers and engineers in the future. The Automotive Open Source Summit is a great place for automotive professionals to gain knowledge, keep up to date with the latest developments, and make valuable connections in the industry.'
categories: []
keywords: ["Open Source Automotive Software","Open Source Automotive Technology","Automotive Software Conference","Automotive Software Development","Vehicle Software Engineering","Automotive Software","Automotive Technology","Automotive Software Communities"]
slug: ""
aliases: []
toc: false
draft: false
hide_page_title: true
hide_sidebar: true
container: container-fluid
layout: single
redirect_url: https://www.eclipse.org
---

