# Contributing to automotive-oss.org

Thanks for your interest in this project.

## Project description

The Automotive Open Source Summit is an annual gathering of senior architects, innovators, and executives from around the world. At this event, attendees come together to discuss the latest trends in automotive grade open source software, network with like-minded professionals, and learn about the various technologies and challenges faced in automotive software development. As the inaugural event, the 2023 Automotive Open Source Summit will be focused around topics for senior architects and executives, with the outlook of expanding the conversation towards developers and engineers in the future. The Automotive Open Source Summit is a great place for automotive professionals to gain knowledge, keep up to date with the latest developments, and make valuable connections in the industry.

* https://gitlab.eclipse.org/eclipsefdn/it/websites/automotive-oss.org

## Developer resources

The project maintains the following source code repositories

* https://gitlab.eclipse.org/eclipsefdn/it/websites/automotive-oss.org

## Eclipse Contributor Agreement

Before your contribution can be accepted by the project team contributors must
electronically sign the Eclipse Contributor Agreement (ECA).

* http://www.eclipse.org/legal/ECA.php

Commits that are provided by non-committers must have a Signed-off-by field in
the footer indicating that the author is aware of the terms by which the
contribution has been provided to the project. The non-committer must
additionally have an Eclipse Foundation account and must have a signed Eclipse
Contributor Agreement (ECA) on file.

For more information, please see the Eclipse Committer Handbook:
https://www.eclipse.org/projects/handbook/#resources-commit

## Contact

Contact the Eclipse Foundation Webdev team via webdev@eclipse-foundation.org.
